<?php
namespace Excellence\Custom\Block\Product;

class Image extends \Magento\Framework\View\Element\Template
{
    public function getCustomImageUrl($link_url = '')
    {
        if (!empty($link_url)) {
            $media_url = $this->_storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA);
            return $media_url . '' . $link_url;
        } else {
            return '#';
        }
    }
}
